package main

import (
	"fmt"
	"math"
	"time"
)

func IsPrimeSimple(value int) bool {
	for i := 2; i <= int(math.Floor(float64(value)/2)); i++ {
		if value%i == 0 {
			return false
		}
	}
	return value > 1
}

func printPrimeSimple(limit int) {
	for i := 1; i <= limit; i++ {
		if IsPrimeSimple(i) {
			fmt.Printf("%v ", i)
		}
	}
	fmt.Println("")
}

func IsPrimeSqrt(value int) bool {
	for i := 2; i <= int(math.Sqrt(float64(value))); i++ {
		if value%i == 0 {
			return false
		}
	}
	return value > 1
}

func printPrimeSqrt(limit int) {
	for i := 1; i <= limit; i++ {
		if IsPrimeSqrt(i) {
			fmt.Printf("%v ", i)
		}
	}
	fmt.Println("")
}

func isPrimeSqrtOptimized(value int) bool {
	switch {
	case value < 2:
		return false
	case value == 2:
		return true
	case value%2 == 0:
		return false
	default:
		for i := 3; i <= int(math.Sqrt(float64(value))); i += 2 {
			if value%i == 0 {
				return false
			}
		}
		return true
	}
}

func printPrimeSqrtOptimized(limit int) {
	for i := 1; i <= limit; i++ {
		if isPrimeSqrtOptimized(i) {
			fmt.Printf("%v ", i)
		}
	}
	fmt.Println("")
}

func printSieveOfEratosthenes(limit int) {
	f := make([]bool, limit+1)
	for i := 2; i <= int(math.Sqrt(float64(limit))); i++ {
		if f[i] == false {
			for j := i * i; j <= limit; j += i {
				f[j] = true
			}
		}
	}
	for i := 2; i <= limit; i++ {
		if f[i] == false {
			fmt.Printf("%v ", i)
		}
	}
	fmt.Println("")
}

func timeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	fmt.Printf("\"%s\" took %s to run.\n\n", name, elapsed)
}

func main() {
	const limit = 997

	startTime := time.Now()
	printPrimeSimple(limit)
	timeTrack(startTime, "Simple")

	startTime = time.Now()
	printPrimeSqrt (limit)
	timeTrack(startTime, "Sqrt")

	startTime = time.Now()
	printPrimeSqrtOptimized(limit)
	timeTrack(startTime, "Sqrt Optimized")

	startTime = time.Now()
	printSieveOfEratosthenes(limit)
	timeTrack(startTime, "Sieve Of Eratosthenes")

}
